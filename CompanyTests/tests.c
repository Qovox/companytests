﻿/** @file tests.c
 * Contains a variety of functions that have been asked to write during 
 * company programming tests.
 *
 * @author Jeremy Lara <jeremy.lara.pro@gmail.com>
 * @version 1.0
 * @date 31/07/2019
 */

/******************************************************************************
 * INCLUDE
 *****************************************************************************/

#include <ctype.h>
#include <math.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/******************************************************************************
 * DEFINE
 *****************************************************************************/

#define TRUE 1
#define FALSE 0

/******************************************************************************
 * VARIABLES & TYPES
 *****************************************************************************/

int from_ids_ntw_0[4] = { 4, 9, 6, 1 };
int to_ids_ntw_0[4]   = { 9, 5, 1, 4 };
int from_ids_ntw_1[8] = { 1, 7, 3, 4, 2, 6, 9 };
int to_ids_ntw_1[8]   = { 3, 3, 4, 2, 6, 9, 5 };

typedef struct node
{
	int value;
	struct node* left;
	struct node* right;
} Node;

/******************************************************************************
 * FUNCTIONS
 *****************************************************************************/

/**
 * Determines the closest value to a given reference value.
 *
 * Finds the closest value to a reference value in an array of values. If two
 * values are found, then the positive or bigger one is returned.
 *
 * @param [in] values Array of values in which to search
 * @param [in] size   The size of the parameter values
 * @param [in] value  The reference value
 * @return The closest value to the parameter value.
 */
int closest_to_value(int values[], int size, int value);

/**
 * Computes the sum of every multiples inferiors to a given number.
 *
 * Given a reference number, computes the sum of every multiples of 3, 5 or 7
 * strictly inferiors to this number.
 *
 * @param [in] n Reference value such as 0 <= n <= 1000
 * @return The sum of every multiples of 3, 5, or 7 strictly inferiors to n.
 */
int compute_multiples_sum(int n);

/**
 * Sums a given number of values using varargs.
 *
 * Sums the number given in parameters. The first one gives the total amount 
 * of numbers to sum.
 *
 * @param [in] count The number of elements to sum
 * @return The sum of all elements
 */
int count(int count, ...);

/**
 * Counts the number of possible pairs given the number of distinct elements.
 *
 * Given the exact number of distinct elements, computes the total number of
 * possible pairs formed with those elements. An element can only be used once.
 * For instance, the following elements: A, B, C and D can give the following
 * pairs: AB, AC, AD, BC, BD, CD and the result should be 6. In that example 
 * the input parameter is 4.
 *
 * @param [in] n The number of distinct element to form pairs with
 * @return The number of pairs that can be formed with n distinct elements
 */
int count_pairs(int n);

/**
 * Finds if a node with a specific value exists.
 *
 * Given a root node and a value to find, finds the first node with such value 
 * in the tree starting from the root node.
 *
 * @param [in] node The root node of the tree where to search
 * @param [in] v    The value of the node to find
 * @return The first node with a given value in the tree of a given root node
 */
Node* find(Node* node, int v);

/**
 * Gets the bit's value of a given number at a given position.
 *
 * Get the value of the bit at a given position (starting from the
 * least significant bit at index 0) of a given number.
 * For instance: With n = 106 (0110 1010) and pos = 3, returns 1 which is the 
 * value of bit number 3 (starting from lsb with index 0).
 *
 * @param [in] n   The number in which to get a bit's value
 * @param [in] pos The position of the bit to get value from
 * @return The value of a specific bit in a given number
 */
int get_bit_value(int n, int pos);

/**
 * Retrieves even pairs of three bits from values from a source array.
 *
 * Takes every even paris of three bits from an source array of bytes and put 
 * them in the destination array.
 *
 * @param [in] src The source array to be treated
 * @param [in] size The source array size
 * @param [in] dest The destination array to three-bits pairs
 */
void get_even_three_bits(char* src, int size, char* dest);

/**
 * Compares two words and indicates if there are twins.
 *
 * Twins are words that contain the same letters, taking in account multiple
 * identical letters. This function does not care about case sensitivity.
 *
 * @param [in] a The first word to compare
 * @param [in] b The second word to compare
 * @return TRUE if parameters a and b are twins, FALSE otherwise.
 * @see #TRUE #FALSE
 */
int is_twin(const char* a, const char* b);

/**
 * Given a string, only prints alpha characters.
 *
 * Filters and prints the alpha characters from the digit ones.
 *
 * @param [in] str The string to print alpha characters from
 */
void print_only_char(const char* str);

/**
 * Given a string, only prints digit characters.
 *
 * Filters and prints the digit characters from the alpha ones.
 *
 * @param [in] str The string to print digit characters from
 */
void print_only_digits(const char* str);

/**
 * Computes sum of number in array within a given range.
 *
 * Sum of range of elements' values starting from index 'from' to index 'to'
 *
 * @param [in] values The array of values to sum within a given range
 * @param [in] size   The size of the array of values
 * @param [in] from   The index of the first value to sum
 * @param [in] to     The index of the last value to sum
 * @return The sum of all the values in between the two indexes
 */
int sum_in_range(int values[], int size, int from, int to);

void fne_parse_file(const char* file_name, int** from_ids, int** to_ids)
{
	FILE* fd;
	fopen_s(&fd, file_name, "r");
	char c = '\0';
	int line = 0;
	int size = 0;
	int count = 0;
	int* from_ids_array = NULL;
	int* to_ids_array = NULL;
	char* number = (char*)calloc(20, sizeof(char) * 20);
	char* number_reset = number;

	while ((c = fgetc(fd)) != EOF)
	{
		if (line == 1)
		{
			if (c == '\n')
			{
				line++;
				*number = '\0';
				number = number_reset;
				size = atoi(number);
				from_ids_array = (int*)malloc(sizeof(int) * size);
				to_ids_array = (int*)malloc(sizeof(int) * size);
			}
			else
			{
				*number++ = c;
			}
		}
		else if (line == 2)
		{
			if (c == ' ')
			{
				*number = '\0';
				number = number_reset;
				*(from_ids_array + count++) = atoi(number);
			}
			else if (c == '\n')
			{
				line++;
				*number = '\0';
				number = number_reset;
				*(from_ids_array + count) = atoi(number);
				count = 0;
			}
			else
			{
				*number++ = c;
			}
		}
		else if (line == 3)
		{
			if (c == ' ')
			{
				*number = '\0';
				number = number_reset;
				*(to_ids_array + count++) = atoi(number);
			}
			else if (c == '\n')
			{
				line++;
				*number = '\0';
				number = number_reset;
				*(to_ids_array + count) = atoi(number);
			}
			else
			{
				*number++ = c;
			}
		}
		else if (c == '\n')
		{
			line++;
		}
	}
	
	*from_ids = from_ids_array;
	*to_ids = to_ids_array;
	free(number);
	fclose(fd);
}

int** find_unique_elements(int* src, int* dest, int size, int* elements_found)
{
	int* unique_elements = (int*)malloc(sizeof(int) * size);
	int* start = unique_elements;

	for (int i = 0; i < size; ++i)
	{
		for (int j = 0; j < size; ++j)
		{
			if (*(dest + i) == *(src + j))
			{
				break;
			}
			else if (j == size - 1)
			{
				*unique_elements++ = *(dest + i);
				*elements_found += 1;
			}
		}
	}

	return &start;
}

int main() {

	/********************
	 * closest_to_value *
	 ********************/

	int closest_values_0[] = { 2, 8, -2, 9, 10, 7, 6 };
	int closest_values_1[] = { 10, 8, -2, 7, 9, 4, 5 };
	int closest_values_2[] = { 8, -3, 9, 7, 5, 3, 10 };
	int closest_values_3[] = { 9, 8, -1, 10, 7, 4, -9 };
	int closest_values_4[] = { 8, 9, -1, 7, 10, 4 };

	printf("closest_to_value(%s, %d, %d) = %d\n", "[2, 8, -2, 9, 10, 7, 6]", 7, 4, closest_to_value(closest_values_0, 7, 4));
	printf("closest_to_value(%s, %d, %d) = %d\n", "[10, 8, -2, 7, 9, 4, 5]", 7, 1, closest_to_value(closest_values_1, 7, 1));
	printf("closest_to_value(%s, %d, %d) = %d\n", "[8, -3, 9, 7, 5, 3, 10]", 7, 0, closest_to_value(closest_values_2, 7, 0));
	printf("closest_to_value(%s, %d, %d) = %d\n", "[9, 8, -1, 10, 7, 4, -9]", 7, -5, closest_to_value(closest_values_3, 7, -5));
	printf("closest_to_value(%s, %d, %d) = %d\n", "[8, 9, -1, 7, 10, 4]", 6, 7, closest_to_value(closest_values_4, 6, 7));

	printf("\n");

	/*********
	 * count *
	 *********/

	printf("count(%d, ...) = %d\n", 0, count(0, 7, -9));
	printf("count(%d, ...) = %d\n", 1, count(1, 4));
	printf("count(%d, ...) = %d\n", 2, count(2, 0, 5));
	printf("count(%d, ...) = %d\n", 3, count(3, 8, -6, 9));

	printf("\n");

	/**********************
	 * compute_join_point *
	 **********************/

	printf("compute_join_point(%d, %d) = %d\n", 471, 480, compute_join_point(471, 480));
	printf("compute_join_point(%d, %d) = %d\n", 22, 5, compute_join_point(22, 5));
	printf("compute_join_point(%d, %d) = %d\n", 68, 68, compute_join_point(68, 68));

	printf("\n");

	/*************************
	 * compute_multiples_sum *
	 *************************/

	printf("compute_multiples_sum(%d) = %d\n", 0, compute_multiples_sum(0));
	printf("compute_multiples_sum(%d) = %d\n", 3, compute_multiples_sum(3));
	printf("compute_multiples_sum(%d) = %d\n", 11, compute_multiples_sum(11));
	printf("compute_multiples_sum(%d) = %d\n", 1000, compute_multiples_sum(1000));

	printf("\n");


	/*************************
	 * find_network_endpoint *
	 *************************/

	int fne_from_ids_0[] = { 4, 9, 6, 1 };
	int fne_to_ids_0[]   = { 9, 5, 1, 4 };
	int fne_from_ids_1[] = { 7, 1, 3, 4, 2, 6, 9 };
	int fne_to_ids_1[]   = { 3, 3, 4, 6, 6, 9, 5 };
	int fne_from_ids_2[] = { 7, 1, 3, 4, 2, 6, 9, 5 };
	int fne_to_ids_2[] = { 3, 3, 4, 6, 6, 9, 5, 7 };
	int* fne_from_ids_3 = NULL;
	int* fne_to_ids_3 = NULL;
	fne_parse_file("big_linear_ntw.txt", &fne_from_ids_3, &fne_to_ids_3);
	
	printf("find_network_endpoint(%d, %d, %s, %s) = %d\n", 6, 4, "[4, 9, 6, 1]", "[9, 5, 1, 4]", find_network_endpoint(6, 4, fne_from_ids_0, fne_to_ids_0));
	printf("find_network_endpoint(%d, %d, %s, %s) = %d\n", 3, 8, "[7, 1, 3, 4, 2, 6, 9]", "[3, 3, 4, 6, 6, 9, 5]", find_network_endpoint(3, 8, fne_from_ids_1, fne_to_ids_1));
	printf("find_network_endpoint(%d, %d, %s, %s) = %d\n", 4, 9, "[7, 1, 3, 4, 2, 6, 9, 5]", "[3, 3, 4, 6, 6, 9, 5, 7]", find_network_endpoint(4, 9, fne_from_ids_2, fne_to_ids_2));
	printf("find_network_endpoint(%d, %d, %s, %s) = %d\n", 1, 4999, "[...]", "[...]", find_network_endpoint(1, 4999, fne_from_ids_3, fne_to_ids_3));

	printf("\n");
	free(fne_from_ids_3);
	free(fne_to_ids_3);

	/*****************
	 * get_bit_value *
	 *****************/

	// 26'970 == 0111 1011 1101 1110
	printf("get_bit_value(%d, %d) = %d\n", 26970, 0, get_bit_value(26970, 0));
	printf("get_bit_value(%d, %d) = %d\n", 26970, 1, get_bit_value(26970, 1));
	printf("get_bit_value(%d, %d) = %d\n", 26970, 2, get_bit_value(26970, 2));
	printf("get_bit_value(%d, %d) = %d\n", 26970, 3, get_bit_value(26970, 3));
	printf("get_bit_value(%d, %d) = %d\n", 26970, 4, get_bit_value(26970, 4));
	printf("get_bit_value(%d, %d) = %d\n", 26970, 5, get_bit_value(26970, 5));
	printf("get_bit_value(%d, %d) = %d\n", 26970, 6, get_bit_value(26970, 6));
	printf("get_bit_value(%d, %d) = %d\n", 26970, 7, get_bit_value(26970, 7));
	printf("get_bit_value(%d, %d) = %d\n", 26970, 8, get_bit_value(26970, 8));
	printf("get_bit_value(%d, %d) = %d\n", 26970, 9, get_bit_value(26970, 9));
	printf("get_bit_value(%d, %d) = %d\n", 26970, 10, get_bit_value(26970, 10));
	printf("get_bit_value(%d, %d) = %d\n", 26970, 11, get_bit_value(26970, 11));
	printf("get_bit_value(%d, %d) = %d\n", 26970, 12, get_bit_value(26970, 12));
	printf("get_bit_value(%d, %d) = %d\n", 26970, 13, get_bit_value(26970, 13));
	printf("get_bit_value(%d, %d) = %d\n", 26970, 14, get_bit_value(26970, 14));
	printf("get_bit_value(%d, %d) = %d\n", 26970, 15, get_bit_value(26970, 15));

	printf("\n");

	/***********
	 * is_twin *
	 ***********/

	printf("is_twin(%s, %s) = %d\n", "aBLoob", "oBbAlo", is_twin("aBLoob", "oBbAlo"));
	printf("is_twin(%s, %s) = %d\n", "length", "problem", is_twin("length", "problem"));
	printf("is_twin(%s, %s) = %d\n", "SsSsSsSs", "sSsSsSsS", is_twin("SsSsSsSs", "sSsSsSsS"));
	printf("is_twin(%s, %s) = %d\n", "nIah", "GnaH", is_twin("nIah", "GnaH"));
	printf("is_twin(%s, %s) = %d\n", "sPike", "Spiky", is_twin("sPike", "Spiky"));

	printf("\n");

	/*******************
	 * print_only_char *
	 *******************/

	printf("print_only_char(%s) = ", "H3110 y0u!");
	print_only_char("H3110 y0u!");

	/*********************
	 * print_only_digits *
	 *********************/

	printf("print_only_digits(%s) = ", "H3110 y0u!");
	print_only_digits("H3110 y0u!");

	/****************
	 * sum_in_range *
	 ****************/

	int sir_0[] = { 7, -1, -3, 4, 2, 6, 9, 8 };
	int sir_1[] = { -3, 3, 4, 6, 6, -9, 5, 7 };

	printf("sum_in_range(%s, %d, %d, %d) = %d\n", "[7, -1, -3, 4, 2, 6, 9, 8]", 8, 1, 6, sum_in_range(sir_0, 8, 1, 6));
	printf("sum_in_range(%s, %d, %d, %d) = %d\n", "[-3, 3, 4, 6, 6, -9, 5, 7]", 8, 7, 7, sum_in_range(sir_1, 8, 7, 7));
	printf("sum_in_range(%s, %d, %d, %d) = %d\n", "[7, -1, -3, 4, 2, 6, 9, 8]", 8, 2, 0, sum_in_range(sir_0, 8, 2, 0));
	printf("sum_in_range(%s, %d, %d, %d) = %d\n", "[-3, 3, 4, 6, 6, -9, 5, 7]", 8, 4, 7, sum_in_range(sir_1, 8, 4, 7));
	printf("sum_in_range(%s, %d, %d, %d) = %d\n", "[7, -1, -3, 4, 2, 6, 9, 8]", 8, 0, 7, sum_in_range(sir_0, 8, 0, 7));
	
	printf("\n");

	return 0;
}

int closest_to_value(int values[], int size, int value)
{
	int closest = values[0];

	for (int i = 1; i < size; ++i)
	{
		if ((abs(values[i] - value) < abs(closest - value)) || ((abs(values[i] - value) == abs(closest - value)) && (closest < values[i])))
		{
			closest = values[i];

			if (closest == value)
			{
				break;
			}
		}
	}

	return closest;
}

int compute_multiples_sum(int n)
{	
	if (n < 3)
	{
		return 0;
	}
	else
	{
		int sum = 3;

		for (int i = 5; i < n; ++i)
		{
			if (i % 3 == 0 || i % 5 == 0 || i % 7 == 0)
			{
				sum += i;
			}
		}
		
		return sum;
	}
}

int count(int count, ...)
{
	int sum = 0;
	va_list args;
	va_start(args, &count);

	while (count-- > 0)
	{
		sum += va_arg(args, int);
	}

	va_end(args);

	return sum;
}

int count_pairs(int n)
{
	return (n * (n - 1)) / 2;
}

Node* find(Node* node, int v)
{
	while (node != NULL)
	{
		if (v > node->value)
		{
			node = node->right;
		}
		else if (v < node->value)
		{
			node = node->left;
		}
		else
		{
			return node;
		}
	}

	return node;
}

int compute_join_point(int s1, int s2)
{
	int sum = 0;
	int num = 0;

	while (s1 != s2)
	{
		if (s1 < s2)
		{
			num = s1;

			while (num != 0)
			{
				sum += num % 10;
				num /= 10;
			}

			s1 += sum;
		}
		else // We know for sure that s1 != s2
		{
			num = s2;

			while (num != 0)
			{
				sum += num % 10;
				num /= 10;
			}

			s2 += sum;
		}

		sum = 0;
	}

	return s1;
}

int find_network_endpoint(int start_node_id, int n, int from_ids[], int to_ids[])
{
	int endpoint = start_node_id;
	int last = start_node_id;
	int found = 0;
	int i;

	do
	{
		for (i = 0; i < n; ++i)
		{
			if (endpoint == from_ids[i])
			{
				last = endpoint;
				endpoint = to_ids[i];
				found = 1;
				break;
			}
			else if (i == (n - 1))
			{
				found = 0;
			}
		}

		if (endpoint == start_node_id)
		{
			return last;
		}
	} while (found);

	return endpoint;
}

int get_bit_value(int n, int pos)
{
	return ((n & (1 << pos)) >> pos);
}

void get_even_three_bits(char* src, int size, char* dest)
{
	for (int i = 0; i < size * 3; ++i)
	{

	}
}

int is_twin(const char* a, const char* b)
{
	size_t length_b = strlen(b);

	if (strlen(a) == length_b)
	{
		int* multiple_letters_indexes = (int*)calloc(length_b, sizeof(char) * length_b);

		if (multiple_letters_indexes != NULL)
		{
			while (*a != '\0')
			{
				char lower_a = tolower(*a);

				for (int i = 0; i < length_b; i++)
				{
					if (lower_a == tolower(*(b + i)) && multiple_letters_indexes[i] == 0)
					{
						multiple_letters_indexes[i] = 1;
						a++;
						break;
					}
					else if (i == (length_b - 1))
					{
						free(multiple_letters_indexes);
						return FALSE;
					}
				}
			}

			free(multiple_letters_indexes);
			return TRUE;
		}
		else
		{
			fprintf(stderr, "Couldn't allocate memory!\n");
			return FALSE;
		}
	}
	else
	{
		return FALSE;
	}
}

void print_only_char(const char* str)
{
	for (; *str; str++)
	{
		if (isalpha(*str))
		{
			printf("%c", *str);
		}
	}

	printf("\n");
}

void print_only_digits(const char* str)
{
	for (; *str; str++)
	{
		if (isdigit(*str))
		{
			printf("%c", *str);
		}
	}

	printf("\n");
}

int sum_in_range(int values[], int size, int from, int to)
{
	int sum = 0;

	for (int i = from; i <= to; ++i)
	{
		sum += values[i];
	}

	return sum;
}